<div class="ui main container">
	<h2 class="ui dividing header">Part Payment</h2>
	<div class="ui segment" style="min-height: 8rem;">
	<h4 class="ui dividing header">Customer Info</h4>

	<form class="ui form billing" id="pay_form">
	 <div class="field">
	    <label>Select Customer</label>
	    <select class="ui search dropdown" name="customer_name" id="cus">
	      <option value="">Select Customer / Phone Number</option>
	      <?php foreach ($customers as $customer) : ?>
	      	
	      <option value="<?php echo $customer->id; ?>"><?php echo $customer->name; ?> - <?php echo $customer->phone; ?></option>

	     <?php endforeach; ?>
	    </select>
	  </div>

	

      <h4 class="ui dividing header">Payment Information</h4>
     
      <div class="three fields">
        <div class="field">
          <label>Credit</label>
          <input type="text" placeholder="" name="credit"  id="credit_amount" readonly="">
        </div>
        <div class="field">
          <label>Cash Recived</label>
          <input type="text" placeholder="cash received" name="cash_received" id="">
        </div>
        <div class="field">
         <br/> 
         <!--  <label>Quantity</label>
          <input type="text" placeholder="Quantity" name="product_quantity"> -->
          <button type="submit" class="ui teal button" id="save" >
			  Save
			</button>

			<a href="<?php echo base_url();?>index.php/Billing"><button  class="ui button" id="cancel">
			  Cancel
			</button></a>
        </div>
        
        
      </div>
    </form>

     

	 

	 
	</div>
</div>

<script> 
$(function(){
	
	 $('#cus').change(function(){
	 	 $.ajax( {
                  url:base_url+'index.php/Partpayment/get_credit',
                  data:$('#pay_form').serialize(),
                   type: 'post',
                  success:function(response) {
                  	 $('[name="credit"]').val(response);
                  	
                  }
               });
      
				  return false;
    });
	 $('#pay_form').submit(function(){
	 		 $.ajax( {
                  url:base_url+'index.php/Partpayment/do_payment',
                  data:$('#pay_form').serialize(),
                   type: 'post',
                  success:function(response) {
                  	
                  	 // alert('Payment Completed');
                  	 $('#pay_form')[0].reset();
                  	
                  }
               });
      
				  return false;
	 });
	

});
</script>