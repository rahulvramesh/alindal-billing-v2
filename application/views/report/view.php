<div class="ui main container">
<label>Total Sales : <b><?php echo count($entries); ?></b></label>
  <table class="ui celled table">
    <thead>
      <tr>
        <th>Name</th>
        <th>Phone</th>
        <th>Discount</th>
        <th>Total</th>
        <th>Payment</th>

      </tr>
    </thead>
    <tbody>
    <?php if(count($entries) <= 0)
          {
            echo " <tr> <td>No Record Found</td></tr>";
          }
          else{
          ?>
    {entries}

      <tr>
        <td>{name}</td>
        <td>{phone}</td>
        <td>{discount}</td>
        <td>{total}</td>
        <td>{payment}</td>
      </tr>

     {/entries}

     <?php } ?>

    </tbody>
  </table>

 <br/>

<form name="cash_on_hand_form" action="<?php echo base_url(); ?>index.php/Report/SaveCashOnHand">
<input type="hidden" name="date" value="<?php echo $_GET['date']; ?>">
 <div class="ui grid">
  <div class="five wide column"></div>
  <div class="five wide column"></div>

  <div class="six wide column" style="float: right;text-align: right;">
    <div class="ui icon input ">
  <input type="text" placeholder="Cash On Hand" name="amount" required="true" value=" <?php echo $cash_on_hand; ?>">

</div>
<button class="ui primary button" id="cash_on_hand">
  Save
</button>
  </div>
</div>
</form>
<!-- loading -->

<br/>
<br/>
 

</div>
