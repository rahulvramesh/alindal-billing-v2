<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

	public function __construct()
    {
           // Call the CI_Model constructor
        parent::__construct();
	   $this->load->model('Report_model');
	   $this->load->library('parser');
	}

	public function index()
	{
		$this->load->view('common/header.php');
		$this->load->view('report/index.php');
		$this->load->view('common/footer.php');
	}

	public function View()
	{
		$date = $this->input->get('date');
		$data['entries'] = $this->Report_model->getDailyReport($date);

		$data['cash_on_hand'] =  $this->Report_model->getCashOnHand($date);

		$this->load->view('common/header.php');
		$this->parser->parse('report/view.php', $data);
		$this->load->view('common/footer.php');
		
	}
	public function cash_in_hand()
	{
		//$amount=$this->input->post('amount');
		// echo $amount;
		//$res = $this->Report_model->enter_cash($amount);
	}

	public function SaveCashOnHand()
	{

		$amount = $this->input->get('amount');
		$date = $this->input->get('date');

		$this->Report_model->cashOnHand($date,$amount);

		$url = base_url()."index.php/Report/View?date=".$date;

		header("Location: ".$url);

	}

}