<?php
class Payment_model extends CI_Model {

	public function get_credit($cus_id)
	{
		$this->db->select('credit');
		$this->db->from('customers');
		$this->db->where('id', $cus_id);
		$query = $this->db->get();
		return $query->result_array();

	}
	public function do_payment($data)
	{
		$this->db->insert('part_payment', $data);
	}
	public function update_credit($where,$cred)
	{
		$this->db->set('credit', $cred);
		$this->db->where('credit', $where);
		$this->db->update('customers');
		return true;
	}
}